(ns kti-telegram.core-test
  (:require [clojure.test :refer :all]
            [environ.core :as environ]
            [kti-telegram.core :refer :all]
            [ring.mock.request :as mock]
            [ring.middleware.json :refer [wrap-json-body]]
            [ring.util.response :as resp]
            [clj-http.client]))

(defn not-found?
  "Tests whether a response is a 404 (not found)"
  [response]
  (= 404 (:status response) 404))

(defn okay?
  "Tests whether a response returns a 200 status"
  [response]
  (= 200 (:status response) 200))

(deftest test-integration
  (testing "Returns 404 on unkown uri"
    (are [uri] (-> (mock/request :get uri) handler not-found?)
      "" "/foo" "foo/bar/baz"))
  (with-redefs [get-listener-secret-url (constantly "secret/path")]
    (testing "Returns 404 on any non-post method for the secret url"
      (are [method] (-> (mock/request method (get-listener-secret-url))
                        handler
                        not-found?)
        :get :put :delete :patch)))
  (testing "Telegram message update flow"
    (let [;; We have the request sent from telegram with a user msg
          teleg-update {"message" {"text" "Foo" "chat" {"id" 7}}}
          request (-> (mock/request :post (get-listener-secret-url))
                      (mock/json-body teleg-update))
          ;; And an atom to capture args sent to kti-execute-action
          args-kti-execute-action (atom nil)
          ;; And the response that will come from kti
          kti-response {:body {:id 1 :reference "Foo"}}
          ;; And an atom to capture args sent to telegram
          args-teleg-send-msg (atom nil)]
      (with-redefs [;; Captures the args sent to teleg-send-msg
                    teleg-send-msg #(reset! args-teleg-send-msg (vec %&))
                    ;; And makes kti answer with this
                    execute-kti-action #(do (reset! args-kti-execute-action (vec %&))
                                            kti-response)]
        ;; And we handle the request comming from a user message
        (is (= {:status 200} (handler request)))
        ;; We see that execute-kti-action was called properly
        (is (= [{:method :post
                 :resource :captured-references
                 :params {:reference "Foo"}}]
               @args-kti-execute-action))
        ;; As well as teleg-send-msg
        (is (= [{:chat-id 7
                 :text (-> kti-response :body json->markdown-block)
                 :parse-mode "Markdown"}]
               @args-teleg-send-msg))))))

(deftest test-wrap-check-uri
  (with-redefs [get-listener-secret-url (constantly "secret-url")]
    (let [uri-handler (wrap-check-uri identity)]
      (is (= (resp/not-found "") (uri-handler (mock/request :post "foo"))))
      (let [req (mock/request :post (get-listener-secret-url))]
        (is (= req (uri-handler req)))))))

(deftest test-wrap-check-method
  (let [method-handler (wrap-check-method identity)]
    (are [m] (= (resp/not-found "") (method-handler (mock/request m "foo")))
      :get :put :delete :patch)
    (let [req (mock/request :post "foo")]
      (is (= req (method-handler req))))))

(deftest test-wrap-check-chat-id
  (with-redefs [is-valid-chat-id? #{"123"}]
    (let [request (assoc-in {} [:body "message" "chat" "id"] "456")]
      (is (= (resp/not-found "") ((wrap-valid-chat-id identity) request))))
    (let [request (assoc-in {} [:body "message" "chat" "id"] "123")]
      (is (= ::response ((wrap-valid-chat-id (constantly ::response)) request))))))

(deftest test-base-handler
  (let [args (atom nil)]
    (with-redefs [teleg-send-msg (fn [& xs] (reset! args (vec xs)))]
      (testing "Returns 200"
        (is (= {:status 200} (base-handler {}))))
      (testing "Sends telegram message"
        (let [request {:body {"message" {"chat" {"id" 999}}}
                       :kti-result {:body "some-json-here"}}]
          (base-handler request)
          (is (= [{:chat-id 999
                   :text (json->markdown-block "some-json-here")
                   :parse-mode "Markdown"}]
                 @args)))))))

(deftest test-wrap-set-kti-action
  (with-redefs [teleg-update->kti-action (constantly ::foo)]
    (let [req (-> (mock/request :post (get-listener-secret-url))
                  (mock/json-body {:bar :baz})
                  ;; Converts body to json
                  ((wrap-json-body identity)))
          id-handler (wrap-set-kti-action identity)]
      (is (= (assoc req :kti-action ::foo) (id-handler req))))))

(deftest test-wrap-set-kti-result
  (let [id-handler (wrap-set-kti-result identity)
        args (atom nil)]
    (with-redefs [execute-kti-action (fn [& xs]
                                       (reset! args (vec xs))
                                       ::foo)]
      (testing "Assocs the result of execute-kti-action to kti-result"
        (is (= {::a ::b :kti-result ::foo} (id-handler {::a ::b}))))
      (testing "Calls execute-kti-action with kti-action values"
        (let [request {:kti-action {:method ::a :resource ::b :params ::C}}]
          (id-handler request)
          (is (= [(request :kti-action)] @args)))))))        

(deftest test-teleg-update->kti-action
  (testing "Telegram usual message maps to post captured reference"
    (is (= {:method :post :resource :captured-references :params {:reference "foo"}}
           (teleg-update->kti-action {"message" {"text" "foo"}})))
    (are [x] (nil? (teleg-update->kti-action x))
      {"message" {}} {} nil "foo")))

(deftest test-get-chat-id-whitelist
  (with-redefs [environ/env {}]
    (is (= nil (get-chat-id-whitelist))))
  (with-redefs [environ/env {:chat-id-whitelist "1"}]
    (is (= #{"1"} (get-chat-id-whitelist))))
  (with-redefs [environ/env {:chat-id-whitelist "123,456"}]
    (is (= #{"123" "456"} (get-chat-id-whitelist)))))

(deftest test-is-valid-chat-id?
  (with-redefs [environ/env {:chat-id-whitelist "123"}]
    (is (is-valid-chat-id? "123"))
    (is (not (is-valid-chat-id? "1")))))

(deftest test-resource->kti-url
  (with-redefs [get-kti-host (constantly "http://foo.com")]
    (is (= "http://foo.com/api/captured-references"
           (resource->kti-url :captured-references)))))

(deftest test-assoc-header-auth
  (are [m] (= {::foo ::bar :headers {"Authorization" "Token abc"}}
              (-> {::foo ::bar} (merge m) (assoc-header-auth "abc")))
    nil {} {:headers {}}))

(deftest test-execute-kti-action
  (let [args (atom nil)]
    (with-redefs [clj-http.client/request (fn [& xs]
                                            (reset! args (vec xs))
                                            ::foo)
                  get-kti-token (constantly "foobar")]
      (testing "Returns the same as the client/request"
        (is (= (execute-kti-action {:resource :a}) ::foo)))
      (testing "Calls request with correct params"
        (execute-kti-action {:method :post :resource ::b :params {::c ::d}})
        (is (= [{:method :post
                 :url (resource->kti-url ::b)
                 :headers (get (assoc-header-auth {} "foobar") :headers)
                 :as :json
                 :form-params {::c ::d}
                 :content-type :json}]
               @args)))
      (testing "Don't pass form-params if get"
        (execute-kti-action {:method :get :resource :a})
        (is (= :get (get-in @args [0 :method])))
        (is (= ::nf (get-in @args [0 :form-params] ::nf)))
        (is (= ::nf (get-in @args [0 :content-type] ::nf)))))))

(deftest test-teleg-send-msg
  (let [args (atom nil)
        send-msg-opts {:chat-id 1 :text "foo" :parse-mode "bar"}]
    (with-redefs [get-teleg-url (constantly "t.com")
                  clj-http.client/request (fn [& xs]
                                            (reset! args (vec xs))
                                            ::foo)]
      (testing "Returns the same as client/request"
        (is (= ::foo (teleg-send-msg send-msg-opts))))
      (testing "Calls request with correct params"
        (teleg-send-msg send-msg-opts)
        (is (= [{:method :post
                 :url "t.com/sendMessage"
                 :form-params {:text "foo" :chat_id 1 :parse_mode "bar"}
                 :content-type :json}]
               @args))))))

(deftest test-json->markdown-block
  (is (= "```json\n\"foo\"\n```"
         (json->markdown-block "foo"))))
