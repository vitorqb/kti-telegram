(defproject kti-telegram "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [ring "1.7.1"]
                 [ring/ring-mock "0.4.0"]
                 [ring/ring-json "0.4.0"]
                 [environ "1.1.0"]
                 [clj-http "3.10.0"]
                 [cheshire "5.8.1"]
                 [org.clojure/tools.logging "0.4.1"]]
  :main ^:skip-aot kti-telegram.core
  :target-path "target/%s"
  :profiles {;; :project/dev is defined here, :profiles/dev in profiles.cljs
             :dev [:profiles/dev :project/dev]
             :project/dev {:plugins [[lein-environ "1.1.0"] [lein-pprint "1.2.0"]]}
             :uberjar {:aot :all}})
