#!/bin/bash -x

USAGE="
$0 <target>
Deploys the kti-telegram to target server.
TARGET MUST be either {user}@{ip} or simple {ip}.
MUST be run at the kti-telegram repo root.
ASSUMES that ./deploy/.env exports all necessary production-ready env vars.
ASSUMES host is a Ubuntu 18.10 x64 with ssh access.
ASSUMES nginx is configured with a self-signed certificate as described in
https://www.digitalocean.com/community/tutorials/how-to-create-a-self-signed-ssl-certificate-for-nginx-in-ubuntu-16-04
"

# Ensures correct usage
[ ! "$#" = "1" ] && echo "$USAGE" >/dev/stderr && exit 1

# Set's target and SERVER_NAME
TARGET="$1"
SERVER_NAME="$(echo "$1" | grep -oP '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$')"

# And the app directory
DIR="/app/releases/$(date +%Y%m%d%H%M%S)"
JAR="${DIR}/kti-telegram.jar"
START="${DIR}/start.sh"
ENV="${DIR}/.env"

# Auxiliary functions
function installJava() {
    ssh "$TARGET" "add-apt-repository ppa:openjdk-r/ppa -y && apt-get update --assume-yes && apt-get install openjdk-11-jdk --assume-yes"
}

function installNginx() {
    ssh "$TARGET" "apt-get update --assume-yes && apt-get install nginx --assume-yes"
}

function build () {
    lein do clean, compile, uberjar
}

function createApiFolder() {
    ssh "$TARGET" "mkdir -p $DIR"
}

function uploadUberjar() {
    scp ./target/uberjar/kti-telegram-*-standalone.jar "${TARGET}:${JAR}"
}

function uploadDotEnv() {
    scp ./deploy/.env "${TARGET}:${ENV}"
}

function uploadStartScript() {
    ssh "$TARGET" "cat >${START} && chmod +x ${START}" <<EOF
#!/bin/bash
source ${ENV} && java -jar ${JAR}
EOF
}

function prepareServerService() {
    ssh "$TARGET" 'cat >/etc/systemd/system/kti-telegram-server.service' <<EOF
[Unit]
Description=KTI Telegram Server

[Service]
Restart=on-failure
WorkingDirectory=${DIR}
ExecStart=${START}

[Install]
WantedBy=multi-user.target
EOF
}

function uploadNginxConfig() {
   ssh "$TARGET" 'cat >/etc/nginx/sites-available/default' <<EOF
server{
  listen 80 default_server;
  listen [::]:80 default_server ipv6only=on;
  server_name $SERVER_NAME;
  return 302 https://\$server_name\$request_uri;
}

server{
  listen 443 ssl http2 default_server;
  listen [::]:443 ssl http2 default_server;
  include snippets/self-signed.conf;
  include snippets/ssl-params.conf;
  access_log /var/log/kti_access.log;
  error_log /var/log/kti_error.log;
  
  location / {
    proxy_pass http://localhost:3000/;
    proxy_set_header Host \$http_host;
    proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto \$scheme;
    proxy_redirect  off;
  }
}
EOF
}

function restartNginxService() {
    ssh "$TARGET" 'systemctl enable nginx'
    ssh "$TARGET" 'systemctl restart nginx'
}

function restartService() {
    ssh "$TARGET" 'systemctl enable kti-telegram-server'
    ssh "$TARGET" 'systemctl restart kti-telegram-server'
}

build \
    && installJava \
    && installNginx \
    && createApiFolder \
    && uploadDotEnv \
    && uploadUberjar \
    && uploadStartScript \
    && uploadNginxConfig \
    && prepareServerService \
    && restartService \
    && restartNginxService
