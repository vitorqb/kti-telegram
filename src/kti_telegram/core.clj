(ns kti-telegram.core
  (:require [ring.adapter.jetty :as jetty]
            [ring.util.response :as resp]
            [ring.middleware.json :refer [wrap-json-body]]
            [environ.core :refer [env]]
            [clj-http.client :as client]
            [cheshire.core :as cheshire]
            [clojure.tools.logging :refer [info]]
            [clojure.string :as str])
  (:gen-class))

;;
;; Helpers/env
;; 
(defn get-listener-secret-url [] (env :listener-secret-url))
(defn get-kti-host [] (env :kti-host))
(defn get-kti-token [] (env :kti-token))
(defn get-teleg-url [] (env :teleg-url))
(defn get-port [] (some-> env :port Integer.))
(defn get-chat-id-whitelist [] (some-> env :chat-id-whitelist (str/split #",") set))
(defn is-valid-chat-id? [chat-id]
  (let [chat-id-whitelist (or (get-chat-id-whitelist) #{chat-id})]
    (chat-id-whitelist chat-id)))
(defn teleg-method-url [method] (str (get-teleg-url) "/" method))

(defn resource->kti-url
  "Transforms a keyword representing a kti resource into it's respective url"
  [r]
  (str (get-kti-host) "/api/" (name r)))

(defn assoc-header-auth
  "Associates the Authorization header to a request opts"
  [req-opts token]
  (assoc-in req-opts [:headers "Authorization"] (str "Token " token)))

(defn json->markdown-block [s]
  (str "```json\n" (cheshire/generate-string s {:pretty true}) "\n```"))

;;
;; Kti action
;;
(defn teleg-update->kti-action
  "Transforms a TelegramUpdate into a KtiAction"
  [teleg-update]
  (if-let [text (get-in teleg-update ["message" "text"])]
    {:method :post :resource :captured-references :params {:reference text}}))

(defn execute-kti-action
  "Executes a kti-action by sending the request to the kti host."
  [{:keys [method resource params] :as kti-action}]
  (-> {:url (resource->kti-url resource) :method method :as :json}
      (assoc-header-auth (get-kti-token))
      (cond-> (#{:post :put} method)
        (assoc :form-params params :content-type :json))
      client/request))

;;
;; Telegram 
;;
(defn teleg-send-msg
"Sends a telegram message."
  [{:keys [chat-id text parse-mode]}]
  (-> {:method :post
       :url (teleg-method-url "sendMessage")
       :form-params {:text text :chat_id chat-id}
       :content-type :json}
      (cond-> parse-mode (assoc-in [:form-params :parse_mode] parse-mode))
      client/request))

;;
;; Handlers/Wrappers
;;
(defn wrap-set-kti-action
  "Request wrapper that associates `:kti-action` to the request."
  [handler]
  (fn [request] (-> request
                    :body
                    teleg-update->kti-action
                    (->> (assoc request :kti-action))
                    handler)))

(defn wrap-set-kti-result
  "Request wrapper that associates a `:kti-result` to the request."
  [handler]
  (fn [request]
    (let [kti-result (-> request :kti-action execute-kti-action)
          new-request (assoc request :kti-result kti-result)]
      (handler new-request))))

(defn wrap-check-uri
  "Wrapper to check that the uri is a valid uri"
  [handler]
  (fn [{:keys [uri] :as request}]
    (if (= uri (get-listener-secret-url)) (handler request) (resp/not-found ""))))

(defn wrap-check-method
  "Wrapper to check the method of the request."
  [handler]
  (fn [{:keys [request-method] :as request}]
    (if (= request-method :post) (handler request) (resp/not-found ""))))

(defn wrap-log-request
  "Wrapper that logs the raw incoming request"
  [handler]
  (fn [{:keys [body] :as request}]
    (info "LOGGING INCOMING REQUEST: " request)
    (when (or (map? body) (string? body)) (info" WITH BODY " body))
    (handler request)))

(defn wrap-valid-chat-id
  "Wrapper to check that the chat id is whitelisted"
  [handler]
  (fn [request]
    (if (-> request (get-in [:body "message" "chat" "id"]) str is-valid-chat-id?)
      (handler request)
      (resp/not-found ""))))

(defn base-handler [request]
  (teleg-send-msg {:chat-id (get-in request [:body "message" "chat" "id"])
                   :text (-> request :kti-result :body json->markdown-block)
                   :parse-mode "Markdown"})
  {:status 200})

(def handler (-> base-handler
                 wrap-set-kti-result
                 wrap-set-kti-action
                 wrap-valid-chat-id
                 wrap-check-uri
                 wrap-check-method
                 wrap-log-request
                 wrap-json-body))

;;
;; Main
;; 
(defn -main [& args]
  (jetty/run-jetty handler {:port (or (get-port) 3000)}))
